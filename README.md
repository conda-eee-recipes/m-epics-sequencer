m-epics-sequencer conda recipe
==============================

Home: https://bitbucket.org/europeanspallationsource/m-epics-sequencer

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS sequencer module
