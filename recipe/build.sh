#!/bin/bash

echo "Build seq"
# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
make PROJECT=seq LIBVERSION=2.1.10
make PROJECT=seq LIBVERSION=2.1.10 install

# Clean builddir between variants builds
rm -rf builddir
